package org.kuka.httpdemo;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.socket.LayeredConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

public class Demo {

    private static String URL = "https://de.www-wp-mchuat.uk.aswatson.net/blog";

    public static void main(String[] args) throws Exception {
        LayeredConnectionSocketFactory socketFactory = SocketFactoryHolder.getInstance();

        CloseableHttpClient httpclient = HttpClients
                .custom()
                .setSSLSocketFactory(socketFactory)
                .setSSLHostnameVerifier( NoopHostnameVerifier.INSTANCE).build();

        HttpGet httpGet = new HttpGet(URL);
        CloseableHttpResponse response = httpclient.execute(httpGet);
        try {
            System.out.println(response.getStatusLine());
            HttpEntity entity = response.getEntity();
            EntityUtils.consume(entity);
        }
        finally {
            response.close();
        }
    }


}
