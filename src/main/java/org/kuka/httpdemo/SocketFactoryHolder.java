package org.kuka.httpdemo;

import org.apache.http.conn.socket.LayeredConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.SSLContexts;

import javax.net.ssl.SSLContext;

public class SocketFactoryHolder {

    private static LayeredConnectionSocketFactory instance;

    public static LayeredConnectionSocketFactory getInstance() throws Exception {
        if (instance==null)
            instance = getHttpClientConnectionManager();

        return instance;
    }

    private static LayeredConnectionSocketFactory getHttpClientConnectionManager() throws Exception {
        SSLContextBuilder builder = SSLContexts.custom();
        builder.loadTrustMaterial(null, (TrustStrategy) (chain, authType) -> true);

        SSLContext sslContext = builder.build();
        return new SSLConnectionSocketFactory(sslContext,  NoopHostnameVerifier.INSTANCE);
    }
}
